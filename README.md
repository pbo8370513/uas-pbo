# UAS PBO

## Aplikasi
[Agoda]

## No.1
**peringkat dari produk Agoda Reservation System:**

1. **Use Case: Pencarian Akomodasi**
   - Aktor: Pengguna
   - Deskripsi: Memungkinkan pengguna untuk mencari akomodasi yang tersedia di kota tertentu, memilih hotel, dan memilih jenis kamar.

2. **Use Case: Reservasi**
   - Aktor: Pengguna
   - Deskripsi: Memungkinkan pengguna untuk melakukan reservasi dengan menyediakan nama, nomor telepon, tanggal reservasi, dan tanggal checkout. Pengguna dapat memilih kota, hotel, dan jenis kamar untuk reservasi.

3. **Use Case: Lihat Reservasi**
   - Aktor: Pengguna
   - Deskripsi: Memungkinkan pengguna untuk melihat reservasi yang telah mereka buat dengan memasukkan nama mereka. Sistem akan mengambil dan menampilkan detail reservasi pengguna, termasuk detail akomodasi, jenis kamar, tanggal mulai, tanggal selesai, dan total harga.

4. **Use Case: Pembayaran**
   - Aktor: Pengguna
   - Deskripsi: Memungkinkan pengguna untuk melakukan pembayaran untuk reservasi tertentu. Pengguna dapat memilih dari berbagai metode pembayaran yang tersedia, seperti transfer bank, OVO, DANA, LinkAja, PayPal, atau kartu kredit.


**Peringkat Use Case:**

1. **Pencarian Akomodasi**
   - Penting: Tinggi
   - Penjelasan: Kemampuan untuk mencari akomodasi yang tersedia sangat penting bagi pengguna untuk menemukan dan memilih opsi akomodasi sesuai dengan preferensi dan kebutuhan mereka.

2. **Reservasi**
   - Penting: Tinggi
   - Penjelasan: Melakukan reservasi adalah fungsionalitas inti dari sistem ini. Memungkinkan pengguna untuk memesan akomodasi di hotel dan jenis kamar yang mereka inginkan merupakan hal yang sangat penting untuk kesuksesan produk.

3. **Lihat Reservasi**
   - Penting: Sedang
   - Penjelasan: Melihat reservasi penting bagi pengguna agar mereka dapat melacak pemesanan dan tetap mendapatkan informasi tentang kunjungan yang akan datang. Meskipun tidak sekritikal membuat reservasi, fitur ini meningkatkan pengalaman dan kenyamanan pengguna.

4. **Pembayaran**
   - Penting: Sedang
   - Penjelasan: Melakukan pembayaran diperlukan untuk menyelesaikan proses reservasi. Namun, fitur ini mungkin memiliki prioritas yang lebih rendah dibandingkan dengan use case lainnya karena lebih merupakan langkah transaksional dalam alur pemesanan keseluruhan.

Penting untuk mempertimbangkan konteks dan audiens target produk saat mengutip use case. Selain itu, umpan balik pengguna yang berkelanjutan dan analisis data dapat membantu menyempurnakan peringkat use case agar lebih sesuai dengan kebutuhan dan preferensi pengguna.

## No.2
**Use case table**

| No | Aktivitas | Skala Prioritas | User / Admin |
| -- | ------ | ------ | ------ |
|  1 | Pencarian Akomodasi  ✓                                                        | Tinggi 85%                | User          |
|  2 | Reservasi Akomodasi  ✓                                                        | Tinggi 90%                | User          |
|  3 | Pembayaran  ✓                                                                 | Tinggi 95%                | User          |
|  4 | Kelola Data Pemesanan  ✓                                                      | Tinggi 80%                | Admin         |
|  5 | Kelola Data Akomodasi  ✓                                                      | Tinggi 80%                | Admin         |
|  6 | Verifikasi Pembayaran                                                         | Sedang 75%                | User          |
|  7 | Kelola Data Pengguna  ✓                                                       | Tinggi 80%                | Admin         |
|  8 | Notifikasi Reservasi                                                          | Sedang 70%                | User          |
|  9 | Pencarian Berdasarkan Kategori                                                | Sedang 70%                | User          |
| 10 | Ulasan dan Penilaian Akomodasi  ✓                                             | Sedang 75%                | User          |
| 11 | Penanganan Keluhan Pelanggan                                                  | Tinggi 80%                | Admin         |
| 12 | Integrasi Dengan Sistem Pembayaran  ✓                                         | Tinggi 85%                | Admin         |
| 13 | Kelola Ketersediaan Akomodasi  ✓                                              | Tinggi 90%                | Admin         |
| 14 | Pengelolaan Promosi  ✓                                                        | Tinggi 85%                | Admin         |
| 15 | Pemesanan Layanan Tambahan                                                    | Sedang 70%                | User          |
| 16 | Sistem Poin Atau Reward                                                       | Sedang 70%                | User          |
| 17 | Pencarian Akomodasi Berdasarkan Harga                                         | Tinggi 80%                | User          |
| 18 | Penjadwalan Ulang Pemesanan                                                   | Sedang 75%                | User          |
| 19 | Sistem Notifikasi Harga Terbaik                                               | Sedang 70%                | User          |
| 20 | Integrasi dengan peta dan navigasi                                            | Sedang 70%                | User          |
| 21 | Pencarian Akomodasi Berdasarkan Fasilitas                                     | Sedang 70%                | User          |
| 22 | Kelola Ulasan Pengguna  ✓                                                     | Sedang 70%                | Admin         |
| 23 | Penjadwalan Pengingat Reservasi                                               | Rendah 60%                | User          |
| 24 | Pencarian Akomodasi Dengan Aksesibilitas                                      | Sedang 70%                | User          |
| 25 | Integrasi Dengan Sistem Transportasi                                          | Tinggi 80%                | User          |
| 26 | Layanan Pelanggan 24/7                                                        | Tinggi 85%                | User          |
| 27 | Integrasi Dengan Platform Perjalanan Lainnya                                  | Tinggi 85%                | User          |
| 28 | Kelola Kebijakan Pembatalan                                                   | Tinggi 90%                | Admin         |
| 29 | Integrasi Dengan Sistem Loyalty Program                                       | Sedang 75%                | User          |
| 30 | Pencarian Akomodasi Berdasarkan Aktivitas                                     | Sedang 70%                | User          |
| 31 | Pemberitahuan Perubahan Harga                                                 | Sedang 75%                | User          |
| 32 | Kelola Data Mitra Akomodasi                                                   | Sedang 65%                | Admin         |
| 33 | Fitur Wishlist Atau Daftar Favorit                                            | Sedang 70%                | User          |
| 34 | Sistem Rekomendasi Akomodasi                                                  | Sedang 75%                | User          |
| 35 | Kelola Penawaran Spesial Dan Diskon                                           | Tinggi 80%                | Admin         |
| 36 | Fitur Akomodasi Berdasarkan Tanggal                                           | Sedang 75%                | User          |
| 37 | Kelola Data Tagihan Dan Faktur                                                | Tinggi 85%                | Admin         |
| 38 | Fitur Perbandingan Harga Dengan Pesaing                                       | Sedang 75%                | User          |
| 39 | Kelola Program Aflliasi                                                       | Tinggi 80%                | Admin         |
| 40 | Fitur Pencarian Akomodasi Dekat Landmark                                      | Sedang 70%                | User          |
| 41 | Fitur Penyimpanan Preferensi Pengguna                                         | Sedang 70%                | User          |
| 42 | Kelola Laporan Dan Analisis Data                                              | Tinggi 85%                | Admin         |
| 43 | Pemesanan Akomodasi Dengan Voucher                                            | Tinggi 80%                | User          |
| 44 | Kelola Data Lokasi Akomodasi                                                  | Sedang 75%                | Admin         |
| 45 | Fitur Pemesanan Akomodasi Untuk Acara Khusus                                  | Tinggi 85%                | User          |
| 46 | Kelola Program Kemitraan                                                      | Tinggi 80%                | Admin         |
| 47 | Fitur Pemesanan Dengan Pembayaran Cicilan                                     | Tinggi 80%                | User          |
| 48 | Kelola Data Tarif Dan Harga Akomodasi                                         | Tinggi 85%                | Admin         |
| 49 | Fitur Rekomendasi Berdasarkan Riwayat Pemesanan                               | Sedang 75%                | User          |
| 50 | Fitur Notifikasi Perubahan Harga                                              | Sedang 70%                | User          |
| 51 | Kelola Data Ulasan Dan Penilaian Pengguna  ✓                                  | Sedang 75%                | Admin         |
| 52 | Kelola Program Reward Dan Loyalitas                                           | Tinggi 80%                | Admin         |
| 53 | Kelola Data Kebijakan Pembayaran                                              | Tinggi 85%                | Admin         |
| 54 | Kelola Data Kebijakan Pembatalan Akomodasi                                    | Tinggi 85%                | Admin         |
| 55 | Kelola Data Promo Dan Diskon  ✓                                               | Tinggi 85%                | Admin         |
| 56 | Fitur Pemesanan Akomodasi Dengan Opsi Makanan                                 | Sedang 75%                | User          |
| 57 | Kelola Data Kebijakan Privasi Dan Keamanan                                    | Tinggi 80%                | Admin         |
| 58 | Kelola Data Kebijakan Pengembalian Dana                                       | Tinggi 80%                | Admin         |
| 59 | Kelola Data Kebijakan Pembayaran Khusus                                       | Tinggi 85%                | Admin         |
| 60 | Kelola Data Kebijakan Pemesanan Khusus                                        | Tinggi 80%                | Admin         |
| 61 | Kelola Data Kebijakan Penghapusan Data Pengguna                               | Tinggi 80%                | Admin         |
| 62 | Kelola Data Kebijakan Keamanan Akun Pengguna                                  | Tinggi 80%                | Admin         |
| 63 | Kelola Data Kebijakan Keamanan Pembayaran                                     | Tinggi 80%                | Admin         |
| 64 | Kelola Data Kebijakan Privasi Pengguna                                        | Tinggi 80%                | Admin         |
| 65 | Fitur Pemesanan Akomodasi Dengan Layanan 24 Jam                               | Sedang 75%                | User          |
| 66 | Kelola Data Kebijakan Pembatalan Dan Pengembalian Dana                        | Tinggi 80%                | Admin         |
| 67 | Kelola Data Kebijakan Penggunaan Poin Reward                                  | Tinggi 80%                | Admin         |
| 68 | Kelola Data Kebijakan Pengguna Kupon Diskon                                   | Tinggi 80%                | Admin         |
| 69 | Pencarian Akomodasi Dekat Pusat Kota                                          | Sedang 75%                | User          |
| 70 | Fitur Pemesanan Akomodasi Dengan Layanan Antar Makanan                        | Sedang 75%                | User          |
| 71 | Pencarian Akomodasi Dengan Akses Ke Tempat Wisata                             | Sedang 70%                | User          |
| 72 | Pencarian Akomodasi Dengan Jarak Terdekat ke Bandara                          | Sedang 75%                | User          |
| 73 | Pencarian Akomodasi Dengan Jarak Terdekat Ke Transportasi Umum                | Sedang 75%                | User          |
| 74 | Pencarian Akomodasi Dengan Jarak Terdekat Ke Landmark                         | Sedang 75%                | User          |
| 75 | Pencarian penerbangan berdasarkan lokasi keberangkatan dan tujuan             | Tinggi 90%                | User          |
| 76 | Pilihan penerbangan dengan filter waktu, harga, maskapai, dll.                | Sedang 80%                | User          |
| 77 | Melihat detail penerbangan termasuk jadwal, durasi, dan fasilitas             | Sedang 75%                | User          |
| 78 | Memilih kursi atau kelas penerbangan                                          | Sedang 75%                | User          |
| 79 | Memasukkan informasi penumpang, seperti nama dan tanggal lahir                | Tinggi 80%                | User          |
| 80 | Melakukan pembayaran untuk pemesanan penerbangan                              | Tinggi 90%                | User          |
| 81 | Menerima e-tiket dan konfirmasi penerbangan                                   | Tinggi 85%                | User          |
| 82 | Mengelola pemesanan penerbangan, seperti pengubahan atau pembatalan           | Tinggi 90%                | User          |
| 83 | Memberikan ulasan dan rating terhadap pengalaman penerbangan  ✓               | Sedang 75%                | User          |
| 84 | Melakukan integrasi dengan sistem pihak ketiga, seperti maskapai              | Tinggi 80%                | Admin         |
| 85 | Mengelola data maskapai, jadwal penerbangan, dan harga                        | Tinggi 85%                | Admin         |
| 86 | Memantau ketersediaan dan harga penerbangan  ✓                                | Tinggi 85%                | Admin         |
| 87 | Memberikan notifikasi perubahan jadwal atau informasi penerbangan             | Tinggi 90%                | User          |
| 88 | Mengelola informasi penumpang, seperti paspor atau visa                       | Sedang 75%                | Admin         |
| 89 | Mengakses informasi tentang bagasi, berat maksimum, dan biaya                 | Sedang 70%                | User          |
| 90 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 91 | Melakukan pembaruan dan peningkatan fitur aplikasi                            | Tinggi 80%                | Admin         |
| 92 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 93 | Pencarian destinasi wisata berdasarkan lokasi dan kategori                    | Tinggi 80%                | User          |
| 94 | Menerima tiket dan konfirmasi reservasi wisata                                | Tinggi 85%                | User          |
| 95 | Melakukan integrasi dengan sistem keamanan bandara                            | Tinggi 80%                | Admin         |
| 96 | Mengakses informasi kontak atau layanan pelanggan                             | Sedang 70%                | Admin         |
| 97 | Melakukan integrasi dengan mitra penyedia paket wisata                        | Tinggi 80%                | Admin         |
| 98 | Memantau ketersediaan dan harga paket wisata                                  | Tinggi 85%                | User          |
| 99 | Fitur pemesanan akomodasi dengan layanan rental mobil                         | Sedang 70%                | User          |
| 100 | Pencarian akomodasi dekat area alun-alun atau taman kota bandara             | Sedang 70%                | User          |


**Class Diagram**
![img](Class_Diagram.jpg)

## No.3
**Single Responsibility Principle (SRP):**
Prinsip SRP menyatakan bahwa sebuah kelas seharusnya hanya memiliki satu alasan untuk berubah. Ini berarti setiap kelas seharusnya hanya memiliki satu tanggung jawab dan tidak bertanggung jawab atas beberapa tugas yang tidak terkait.

Pada program, kita memiliki kelas-kelas terpisah untuk User, Location, Accommodation, Room, Reservation, Payment, dan Promotion. Setiap kelas mewakili entitas spesifik dan mengenkapsulasi properti dan perilaku terkait entitas tersebut. Dengan demikian, program mematuhi prinsip SRP, karena setiap kelas memiliki satu tanggung jawab.


**Open/Closed Principle (OCP):**
Prinsip OCP menyatakan bahwa sebuah kelas seharusnya terbuka untuk ekstensi tetapi tertutup untuk modifikasi. Ini berarti kita harus dapat menambahkan fungsionalitas baru ke dalam sebuah kelas tanpa mengubah kode yang sudah ada.

Pada program, kita tidak memiliki ekstensi eksplisit atau antarmuka untuk kelas-kelas tersebut, tetapi struktur kode memungkinkan kita untuk menambahkan kelas atau fitur baru tanpa mengubah kode yang sudah ada. Misalnya, jika kita ingin menambahkan jenis akomodasi atau metode pembayaran baru, kita dapat melakukannya dengan membuat kelas baru tanpa mengubah kode yang sudah ada. Dengan demikian, program memiliki fleksibilitas untuk ekstensi, dan sebagian mematuhi prinsip OCP.


**Liskov Substitution Principle (LSP):**
Prinsip LSP menyatakan bahwa objek dari superclass seharusnya dapat digantikan dengan objek dari subclassnya tanpa mempengaruhi kebenaran program. Dengan kata lain, sebuah subclass seharusnya berperilaku sedemikian rupa sehingga tidak melanggar perilaku yang diharapkan dari superclassnya.

Pada program, kita tidak memiliki hubungan warisan eksplisit di antara kelas-kelas (kecuali untuk PremiumAccommodation), sehingga LSP tidak berlaku secara langsung. Kelas PremiumAccommodation mewarisi dari Accommodation, tetapi menambahkan atribut tambahan (additionalServices). Selama subclass (PremiumAccommodation) tidak merusak perilaku superclassnya (Accommodation) dan kode apa pun yang mengandalkan superclass dapat menangani subclass tanpa masalah, maka kode ini mematuhi prinsip LSP.


**Interface Segregation Principle (ISP):**
Prinsip ISP menyatakan bahwa sebuah kelas tidak boleh dipaksa untuk mengimplementasikan antarmuka yang tidak digunakannya. Ini berarti bahwa antarmuka haruslah spesifik dan fokus pada kebutuhan dari klien.

Pada program, kita tidak memiliki antarmuka eksplisit, sehingga kita tidak dapat menerapkan ISP secara langsung. Namun, jika kode ini berkembang dengan kebutuhan dan antarmuka yang lebih kompleks, akan lebih baik untuk membuat antarmuka yang lebih kecil dan terfokus untuk menghindari memaksa kelas untuk mengimplementasikan metode-metode yang tidak diperlukan.


**Dependency Inversion Principle (DIP):**
Prinsip DIP menyatakan bahwa modul tingkat tinggi tidak boleh bergantung pada modul tingkat rendah. Sebaliknya, keduanya harus bergantung pada abstraksi (antarmuka atau kelas abstrak). Ini mendorong keterhubungan lemah antara kelas-kelas.

Pada program, kita tidak memiliki antarmuka atau abstraksi eksplisit, dan kelas-kelas berinteraksi langsung satu sama lain menggunakan implementasi konkret. Oleh karena itu, prinsip DIP tidak diterapkan secara eksplisit program ini.


## No.4

**Factory Pattern**
Factory Pattern adalah sebuah creational design pattern yang bertujuan untuk mengabstraksi proses pembuatan objek (instansiasi) dari klien sehingga klien tidak perlu tahu secara detail bagaimana objek tersebut dibuat. Dengan menggunakan Factory Pattern, kita dapat memisahkan logika pembuatan objek dari logika penggunaan objek, sehingga memudahkan dalam pengelolaan kode dan perawatan program.

Pada program AgodaReservationSystem, terdapat beberapa kelas yang berperan sebagai creator atau factory untuk menginstansiasi objek-objek yang berbeda. Beberapa kelas tersebut adalah:

User -> Menciptakan objek User
Location -> Menciptakan objek Location
Accommodation -> Menciptakan objek Accommodation
Room -> Menciptakan objek Room
Reservation -> Menciptakan objek Reservation
Payment -> Menciptakan objek Payment
Promotion -> Menciptakan objek Promotion
PremiumAccommodation -> Menciptakan objek PremiumAccommodation

Meskipun pada program, proses pembuatan objek tidak kompleks, namun Factory Pattern tetap dapat memberikan manfaat, terutama jika logika pembuatan objek menjadi lebih kompleks di masa depan atau jika diperlukan fleksibilitas dalam menciptakan variasi objek.

Implementasi Factory Pattern pada program tidak perlu menggunakan class terpisah karena objek yang diciptakan relatif sederhana. Namun, jika program tumbuh lebih kompleks dan terdapat banyak logika pembuatan objek yang berbeda, Factory Pattern akan sangat membantu dalam memisahkan permasalahan tersebut dan meningkatkan code maintainability serta readability.

## No.5

## No.6

## No.7

## No.8

## No.9

## No.10
